import os, datetime
from peewee import *
import peeweedbevolve
from typing import List, Dict
from urllib.parse import urlparse
import requests, json

from Crypto.Hash import SHA256
from Crypto.Random import get_random_bytes

from Server.election import count

if os.environ["APP_ENV"] == "docker":
    DATABASE        = os.environ["POSTGRES_DB"]
    PASSWORD        = os.environ["POSTGRES_PASSWORD"]
    USER            = os.environ["POSTGRES_USER"]
    HOST            = os.environ["POSTGRES_HOST"]
else:
    DATABASE_URL    = urlparse(os.environ["DATABASE_URL"])
    DATABASE        = DATABASE_URL.path[1:]
    PASSWORD        = DATABASE_URL.password
    USER            = DATABASE_URL.username
    HOST            = DATABASE_URL.hostname

length_proposal_phase   = datetime.timedelta(days = int(os.environ["LENGTH_PROPOSAL_PHASE"]))
length_voting_phase     = datetime.timedelta(days = int(os.environ["LENGTH_VOTING_PHASE"]))

database        = PostgresqlDatabase( DATABASE
                                    , password  = PASSWORD
                                    , user      = USER
                                    , host      = HOST
                                    )

class BaseModel(Model):
    class Meta():
        database    = database

class Election(BaseModel):
    id                      = AutoField()
    proposal_phase_start    = DateField()
    winner                  = TextField(null = True)

    def propose ( self
                , title         : str
                , description   : str
                , patch         : str
                ):
        Proposal.create ( election      = self
                        , title         = title
                        , description   = description
                        , patch         = patch
                        )

class User(BaseModel):
    name        = CharField(unique = True, primary_key = True)
    first_name  = TextField()
    last_name   = TextField()
    id          = CharField(unique = True)
    password    = CharField()
    salt        = FixedCharField(max_length = 64)

    def publish(self, title : str, content : str):

        Message.create  ( author            = self
                        , title             = title
                        , content           = content
                        , publishing_date   = datetime.date.today()
                        )
        return True

    def respond(self, title : str, content : str, response_to : int):
        response_to = Message.get_by_id(int(response_to))
        Message.create  ( author            = self
                        , title             = title
                        , content           = content
                        , response_to       = response_to
                        , publishing_date   = datetime.date.today()
                        )
        return True

    def can_authenticate(self, password : str) -> bool:
        password = hash_passwords(password, self.salt)
        if password == self.password:
            return True
        else:
            return False

    def has_voted(self, election : Election):
        return election.participants.select().where(Participant.user == self).exists()

    def vote(self, election : Election, choice : List[str]):
        proposal_phase_end  = election.proposal_phase_start + length_proposal_phase
        voting_phase_end    = proposal_phase_end + length_voting_phase

        if proposal_phase_end < datetime.date.today() and voting_phase_end > datetime.date.today():

            if not self.has_voted(election):
                Vote.create ( election = election
                            , choice = json.dumps(choice)
                            )
                Participant.create  ( election  = election
                                    , user      = self
                                    )
                return True
            else:
                return False
        else:
            return False


    def is_following(self, followed):
        try:
            follows = followed.followers.where(Follower.follower == self).exists()
        except Exception as e:
            raise e
        else:
            return follows

    def follow(self, followed):
        try:
            assert not self.is_following(followed)
            Follower.create ( follower  = self
                            , followed  = followed
                            )
        except AssertionError:
            return False
        except Exception as e:
            raise e
        else:
            return True

    def unfollow(self, followed):
        try:
            assert self.is_following(followed)

            assert Follower.delete().where(Follower.follower == self and Follower.followed == followed).execute() >= 1
        except AssertionError:
            return False
        except Exception as e:
            raise e
            return False
        else:
            return True

class Proposal(BaseModel):
    id          = AutoField()
    election    = ForeignKeyField(Election, backref = "proposals", null = True)
    title       = TextField()
    description = TextField()
    patch       = TextField()
    link        = TextField(null = True)

class Follower(BaseModel):
    id          = AutoField(primary_key = True, unique = True)
    follower    = ForeignKeyField(User, backref = "following")
    followed    = ForeignKeyField(User, backref = "followers")

class Vote(BaseModel):
    election                = ForeignKeyField(Election, backref="votes")
    choice                  = TextField()

class Participant(BaseModel):
    election                = ForeignKeyField(Election, backref="participants")
    user                    = ForeignKeyField(User)

class Message(BaseModel):
    author                  = ForeignKeyField(User, backref = "messages")
    id                      = AutoField(primary_key = True)
    title                   = TextField()
    response_to             = ForeignKeyField('self', backref = "responses", null = True, default = None)
    content                 = TextField()
    publishing_date         = DateTimeField()


def hash_passwords(password : str, salt : str) -> bytes:
    return SHA256.new(data = password.encode("utf-8") + salt.encode("utf-8")).hexdigest()

def register( username      : str
            , first_name    : str
            , last_name     : str
            , id            : str
            , password      : str
            , connected     : bool  = False
            ):
            try:
                if not connected:
                    database.connect()
                id          = hash_passwords(id, "")
                salt        = SHA256.new(data = get_random_bytes(2**3)).hexdigest()
                password    = hash_passwords(password, salt)

                if User.select().where(User.id == id or User.name == user).count() > 0:
                    response    = False
                else:
                    User.create ( name          = username
                                , first_name    = first_name
                                , last_name     = last_name
                                , id            = id
                                , password      = password
                                , salt          = salt
                                )
                    response    = True
                if not connected:
                    database.close()
            except Exception as e:
                raise e
            else:
                return response


def create_tables():
    try:
        assert not os.environ.get("NOT_CREATE_TABLES")
        database.connect()
        database.evolve(interactive = False)
        database.close()
    except AssertionError:
        return False
    except OperationalError:
        return False
    except Exception as e:
        raise e
    else:
        return True

create_tables()
