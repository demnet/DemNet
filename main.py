from flask import Flask, url_for, redirect, request, g, render_template, session
from flask_mistune import Mistune
from functools import wraps
from typing import List
import datetime, json, requests, subprocess
import clock

from peewee import *
from Server.Database import *

SECRET_KEY              = os.environ["SECRET_KEY"]
DEBUG                   = "DEBUG" in os.environ
GITLAB_URI              = os.environ["GITLAB_URI"]
GITLAB_TOKEN            = os.environ["GITLAB_TOKEN"]
GITLAB_WEBHOOK_TOKEN    = os.environ["GITLAB_WEBHOOK_TOKEN"]
DEMNET_ID               = os.environ["DEMNET_ID"]
DEMNET_LOCATION         = os.environ["DEMNET_LOCATION"]
USERS_WITH_ADMIN_ACCESS = os.environ["DEMNET_ADMINS"].split(";")

app = Flask ( __name__
            , static_folder     = "static"
            , static_url_path   = "/static"
            , template_folder   = "output"
            )

app.config.from_object(__name__)

Mistune(app)


## FOR LET'S ENCRYPT CERTIFICATE
@app.route("/.well-known/acme-challenge/Dj8dinV0MAaj9vPzj0uPI0YEGhyZmYJ8CZnkb7XU9_o")
def certificate_authentication():
    return "Dj8dinV0MAaj9vPzj0uPI0YEGhyZmYJ8CZnkb7XU9_o.Qe1Keje4L3boZOoIuMmpxwRieopcYDS-DbLuY1Kovi0"
## END FOR LET'S ENCRYPT CERTIFICATE

@app.template_filter("format_user")
def render_user(user : User, simple = True):
    is_follower = User.get_by_id(session["username"]).is_following(user)
    return render_template  ( "user.html"
                            , user          = user
                            , simple        = simple
                            , is_follower   = is_follower
                            , is_himself    = user.name == session["username"]
                            )

@app.template_filter("format_message")
def render_message(message : Message):
    return render_template("message.html", message = message)

@app.before_request
def before_request():
    g.db    = database
    g.db.connect()

@app.after_request
def after_request(response):
    g.db.close()
    return response


# Routes used by the average users:
def login_required(f):
    @wraps(f)
    def inner(*args, **kwargs):
        if not session.get("authenticated"):
            return redirect("/login")
        return f(*args, **kwargs)
    return inner

@app.route("/login", methods=["POST", "GET"])
def login():
    try:
        if request.method == "GET":
            failed_already  = request.values.get("failed_already") == "true"
            response        = render_template( "login.html", failed_already = failed_already )
        else:
            username        = request.values["username"]
            password        = request.values["password"]
            user            = User.get(User.name == username)
            if user.can_authenticate(password):
                session["authenticated"]    = True
                session["username"]         = user.name
                response                    = redirect("/")
            else:
                raise DoesNotExist()

    except DoesNotExist:
        return redirect(url_for("login", failed_already = "true"))
    except KeyError:
        return "data not provided"
    except Exception as e:
        after_request("")
        raise e
    else:
        return response

@app.route("/logout", methods=["GET"])
@login_required
def logout():
    try:
        session["authenticated"] = False
        session["username"]      = None
    except Exception as e:
        after_request("")
        raise e
    else:
        return redirect("/login")

@app.route("/change_password", methods=["GET", "POST"])
@login_required
def change_password():
    try:
        if request.method == "GET":
            response                = render_template("change_password.html")
        else:
            user                    = User.get(User.name == session["username"])
            password                = request.form["password"]
            password                = hash_passwords(password, user.salt)
            new_passsword           = request.form["new_passsword"]
            new_repeated_password   = request.form["new_repeated_password"]
            if password == user.password:
                if new_passsword == new_repeated_password:
                    new_salt            = SHA256.new(data = get_random_bytes(2**3)).hexdigest()
                    new_passsword       = hash_passwords(new_passsword, new_salt)
                    user.password       = new_passsword
                    user.salt           = new_salt
                    user.save()
                    response            = "Done"
                else:
                    response            = "Passwords don't match"
            else:
                response    = "Invalid current password"
    except KeyError:
        return "Data not provided"
    except Exception as e:
        after_request("")
        raise e
    else:
        return response

@app.route("/", methods=["GET"])
@login_required
def index():
    try:
        feed        = list(Message.select().order_by(-Message.id))
        message     = request.values.get("message")
        response    = render_template("index.html", feed = feed, message = message)
    except KeyError:
        return "data not provided"
    except Exception as e:
        after_request("")
        raise e
    else:
        return response

@app.route("/read/<int:message_id>", methods=["GET"])
@login_required
def read(message_id : str):
    try:
        message         = Message.get(Message.id == message_id)
        response        = render_template   ( "read.html"
                                            , message           = message
                                            , is_author         = message.author.name == session["username"]
                                            )
    except DoesNotExist:
        return "Message id doesn't exists"
    except Exception as e:
        after_request("")
        raise e
    else:
        return response

@app.route("/profile/<username>", methods=["GET"])
@login_required
def profile(username):
    try:
        user        = User.get(User.name == username)
        response    = render_template("profile.html", user = user)
    except DoesNotExist:
        return redirect(url_for("/", message = "User doesn't exist"))
    except Exception as e:
        after_request("")
        raise e
    else:
        return response

@app.route("/follow/<username>", methods=["POST"])
@login_required
def follow(username : str):
    try:
        assert username != session["username"], "same"

        follower    = User.get_by_id(session["username"])
        following   = User.get_by_id(username)

        assert not follower.is_following(following), "already"
        follower.follow(following)

    except AssertionError as e:
        if e.args[0] == "same":
            return redirect(url_for( "index", message = "You can't follow yourself" ))
        else:
            return redirect(url_for( "index", message = "You are already following this user"))
    except DoesNotExist:
        return redirect(url_for( "index", message = "You want to follow a user, that doesn't exist." ))
    except Exception as e:
        after_request("")
        raise e
    else:
        return redirect(url_for( "index", message = f"You are following {username}" ))

@app.route("/unfollow/<username>", methods=["POST"])
@login_required
def unfollow(username : str):
    try:
        follower    = User.get_by_id(session["username"])
        followed    = User.get_by_id(username)
        assert follower.is_following(followed)

        assert follower.unfollow(followed)
    except AssertionError:
        return redirect(url_for("index", message = "Can't unfollow, whom you didn't follow"))
    except DoesNotExist:
        return redirect(url_for("index", message = "Can't unfollow someone, who doesn't exist"))
    except Exception as e:
        after_request("")
        raise e
    else:
        return redirect(url_for("index", message = f"You no longer follow {username}"))

@app.route("/publish", methods=["POST","GET"])
@login_required
def publish():
    try:
        if request.method == "GET":
            response    = render_template( "publish.html", response_to = None )
        else:
            title       = request.form["title"]
            content     = request.form["content"]
            author      = User.get_by_id( session["username"] )

            author.publish(title, content)
            response    = redirect("/")
    except KeyError:
        return "data not provided or not logged in"
    except Exception as e:
        after_request("")
        raise e
    else:
        return response

@app.route("/publish/<int:response_to>", methods=["GET", "POST"])
def respond(response_to : int):
    try:
        if request.method == "GET":
            response    = render_template( "publish.html", response_to = response_to )
        else:
            title       = request.form["title"]
            content     = request.form["content"]
            author      = User.get_by_id(session["username"])
            author.respond( title, content, response_to )
            response    = redirect(url_for( "index", message = "You have responded" ))
    except DoesNotExist:
        return redirect(url_for( "index", message = "You can't respond to a message, that doesn't exist" ))
    except Exception as e:
        after_request("")
        raise e
    else:
        return response

@app.route("/unpublish/<int:message_id>", methods=["POST"])
@login_required
def unpublish(message_id : int):
    try:
        message = Message.get_by_id(message_id)
        if session["username"] == message.author.name:
            message.delete_instance()
            response    = "Done"
        else:
            response    = "You don't have the right to do this"
    except DoesNotExist:
        return "Doesn't exist"
    except IntegrityError:
        return "You can't delete an article, that is subject of debate."
    except Exception as e:
        after_request("")
        raise e
    else:
        return response

@app.route("/vote", methods=["GET", "POST"])
@login_required
def vote():
    try:
        user                = User.get_by_id(session["username"])
        election            = Election.select().order_by(-Election.proposal_phase_start).get()

        proposal_phase_end  = election.proposal_phase_start + length_proposal_phase
        voting_phase_end    = proposal_phase_end + length_voting_phase

        can_vote            = proposal_phase_end < datetime.date.today() and voting_phase_end > datetime.date.today() and (not user.has_voted(election))

        if request.method == "GET":
            can_propose         = election.proposal_phase_start <= datetime.date.today() and proposal_phase_end > datetime.date.today()

            if can_propose:
                merge_requests  = requests.get  ( f"{GITLAB_URI}/projects/{DEMNET_ID}/merge_requests?state=opened&wip=no"
                                                , headers   = { "Private-Token" : GITLAB_TOKEN }
                                                ).json()
            else:
                merge_requests      = None

            response    = render_template   ( "vote.html"
                                            , can_vote              = can_vote
                                            , can_propose           = can_propose
                                            , proposal_phase_end    = proposal_phase_end
                                            , voting_phase_end      = voting_phase_end
                                            , merge_requests        = merge_requests
                                            , election              = election
                                            )
        elif request.method == "POST":
            choice      = json.loads(request.values["vote"])
            if user.vote(election, choice):
                response    = redirect(url_for("index", message = "Voted"))
            else:
                response    = redirect(url_for("index", message = "Either you have voted already or selected the wrong election"))

    except AssertionError:
        return redirect(url_for("index", message = "Invalid Merge Request selected. Resolve all conflicts please."))
    except DoesNotExist:
        return redirect(url_for("index", message = "Election not found"))
    except ValueError:
        return redirect(url_for("index", message = "Data format invalid"))
    except KeyError:
        return redirect(url_for("index", messsage = "Data not provided"))
    except Exception as e:
        after_request("")
        raise e
    else:
        return response

@app.route("/vote/propose", methods = ["POST"])
def propose():
    try:
        user                = User.get_by_id(session["username"])
        election            = Election.select().order_by(-Election.proposal_phase_start).get()

        mr_iid          = request.values["merge_request_iid"]
        merge_request   = requests.get  (f"{GITLAB_URI}/projects/{DEMNET_ID}/merge_requests/{mr_iid}"
                                        , headers   = { "Private-Token" : GITLAB_TOKEN }
                                        )

        merge_request   = merge_request.json()
        assert merge_request["merge_status"] == "can_be_merged" and merge_request["target_branch"] == "master"

        title           = merge_request["title"]
        description     = merge_request["description"]
        patch           = requests.get(f"{DEMNET_LOCATION}/-/merge_requests/{mr_iid}.patch").text

        clock.init()

        assert subprocess.run   ( ["git apply --check --whitespace=fix"]
                                , input = patch.encode("utf-8")
                                , shell = True
                                , cwd   = os.environ["GIT_REPOSITORY"]
                                ).returncode == 0

        proposal        = election.propose  ( title         = title
                                            , description   = description
                                            , patch         = patch
                                            )

        user.publish(title, description + f"\n\nThis is a propsal for the current election")

        response        = redirect(url_for("index", message = "Your proposal was made"))
    except AssertionError:
        return redirect(url_for("index", message = "Invalid Merge Request selected. Resolve all conflicts please. (git apply --check --whitespace=fix failed)"))
    except Exception as e:
        after_request("")
        raise e
    else:
        return response

# ADMIN ONLY
@app.route("/register", methods=["POST","GET"])
@login_required
def register_route():
    try:
        if session["username"] in USERS_WITH_ADMIN_ACCESS:
            if request.method == "GET":
                response    = render_template("register.html")
            else:
                username    = request.values["username"]
                first_name  = request.values["first_name"]
                last_name   = request.values["last_name"]
                id          = request.values["id"]
                password    = request.values["password"]
                response    = str(register(username, first_name, last_name, id, password, connected = True ))
        else:
            response    = "invalid user"
    except KeyError:
        return "data not provided"
    except Exception as e:
        after_request("")
        raise e
    else:
        return response

# CREATING ELECTIONS
def create_election ( title         : str
                    , description   : str
                    , link          : str
                    , id            : int
                    ):
    try:
        creation_date           = datetime.date.today()
        openning_ballot_date    = creation_date + datetime.timedelta( weeks = 4 )
        closing_date            = creation_date + datetime.timedelta( weeks = 6 )

        Election.create ( id                    = id
                        , title                 = title
                        , description           = description
                        , link                  = link
                        , creation_date         = creation_date
                        , openning_ballot_date  = openning_ballot_date
                        , closing_date          = closing_date
                        )

        response                = "Done"
    except KeyError:
        return "data not provided"
    except Exception as e:
        raise e
    else:
        return response
