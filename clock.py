from apscheduler.schedulers.blocking import BlockingScheduler
from Server.Database import *
from Server.election import count
from datetime import date
import json, requests, subprocess

GIT_REPOSITORY                      = os.environ["GIT_REPOSITORY"]
GIT_ACCESS_TOKEN                    = os.environ["GIT_ACCESS_TOKEN"]
DEMNET_LOCATION_WITHOUT_PROTOCOL    = os.environ["DEMNET_LOCATION_WITHOUT_PROTOCOL"]

sched   = BlockingScheduler()

def init():
    subprocess.run( [ "bash", "init_git.sh" ]
                    , env = { "DEMNET_LOCATION_WITHOUT_PROTOCOL" : DEMNET_LOCATION_WITHOUT_PROTOCOL
                            , "GIT_ACCESS_TOKEN" : GIT_ACCESS_TOKEN
                            , "GIT_REPOSITORY" : GIT_REPOSITORY
                            }
                )

@sched.scheduled_job('interval', days = 1, timezone = 'utc')
def check_elections():
    init()

    elections   = (Election.select()
                    .where(Election.winner == None)
                    .where(datetime.date.today() > (Election.proposal_phase_start + length_proposal_phase +length_voting_phase))
                    .order_by(+Election.proposal_phase_start)
                  )

    for election in elections:
        users_count = User.select().count()
        options     = list(map(lambda p: p.id, election.proposals))
        votes       = list(map(lambda v: [ int(v_) for v_ in  json.loads(v.choice)][::-1], election.votes))

        results     = count(votes, options, all_participants = users_count)
        winner      = results["winner"]

        election.winner = winner
        election.save()
        # Automatically merge and the CI will take care of the rest.
        if winner != "NoneOfTheOtherOptions":
            proposal    = Proposal.get_by_id(winner)
            print(f"Election result: {proposal.title} won the election from {election.proposal_phase_start}")

            code    =   subprocess.run( [ "git am --whitespace=fix -s && git push"
                                        ]
                                        , input = proposal.patch.encode("utf-8")
                                        , cwd   = GIT_REPOSITORY
                                        , shell = True
                                    ).returncode

        else:

            print(f"Election result: NoneOfTheOtherOptions won the election from {election.proposal_phase_start}")
            election.winner     = winner

def run():
    init()

    def create_new_election():
        database.connect()
        if not (Election.select()
                        .where(datetime.date.today() >= Election.proposal_phase_start)
                        .where(datetime.date.today() < (Election.proposal_phase_start + length_proposal_phase + length_voting_phase))
                        .exists()
            ):
            current_election    = Election.create(proposal_phase_start = datetime.date.today(), id = Election.select().order_by(-Election.id).get().id + 1)
        else:
            current_election    = (Election.select()
                                            .where(datetime.date.today() >= Election.proposal_phase_start)
                                            .where(datetime.date.today() < (Election.proposal_phase_start + length_proposal_phase + length_voting_phase))
                                            .order_by(-Election.proposal_phase_start)
                                            .get()
                                )
        database.close()
        return current_election

    current_election    = create_new_election()

    sched.add_job(create_new_election, "date", run_date = (current_election.proposal_phase_start + length_proposal_phase + length_voting_phase + datetime.timedelta(days = 1)))
    sched.start()

if __name__ == "__main__":

    run()
