#
# GUNICORN CONFIG FILE
#
import os

keyfile     = os.environ["KEY_FILE"]
certfile    = os.environ["CERT_FILE"]
